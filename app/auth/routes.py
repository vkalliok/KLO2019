from flask import render_template, flash, redirect, request, url_for
from app.auth import bp

from app.auth.forms import LoginForm
from flask_login import current_user, login_user, logout_user
from app.models import User
from werkzeug.urls import url_parse


@bp.route('/login', methods=['GET', 'POST'])
def login():
    """ User login """
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))

    form = LoginForm()
    if form.validate_on_submit():
        account = User.query.filter_by(username=form.username.data).first()

        if account is None or not account.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))

        login_user(account, remember=form.remember_me.data)

        original_target = request.args.get('next')

        if not original_target or url_parse(original_target) != '':
            original_target = url_for('main.index')

        return redirect(original_target)

    return render_template('login.html', title='Sign In', form=form)


@bp.route('/logout')
def logout():
    """ User logout """
    logout_user()
    return redirect(url_for('main.index'))
