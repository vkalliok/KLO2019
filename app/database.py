from flask_sqlalchemy import SQLAlchemy
""" Additional script for importing database, needed to avoid import conflicts. """
db = SQLAlchemy()