#!/bin/bash
samples="resource-samples/results/*/*"
output="./resource-cortex-analysis/cortex-results"
toolscript="resource-samples/tools/cortex/python-tools/cortex_tool.py"

set -e
git clone resource-samples resource-cortex-analysis

for d in /opt/Cortex-Analyzers/analyzers/*/*; do
  cp -r $d resource-samples/tools/cortex/python-tools/
done

if [ ! -d "$output" ]; then
    mkdir $output
fi


git config --global user.email "nobody@testi.testi"
git config --global user.name "Concourse"

for filename in $samples
do
    echo $filename
    if [ "${filename##*.}" = "json" ]; then
        directory=$(basename ${filename%.*})
        python $toolscript $filename $output/$directory
    fi
done

cd resource-cortex-analysis
git add .
git commit -m "cortex-results"

