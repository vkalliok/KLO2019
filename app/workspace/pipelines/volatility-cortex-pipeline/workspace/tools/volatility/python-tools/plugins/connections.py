from util import execute_plugin

def connections(sample, profile):
    """ Execute all Volatility plugins pertaining to connections on given memory sample

    Keyword arguments:
    sample: path to memory sample
    profile: memory profile of the sample

    :return: dictionary containing Volatility JSON output from the executed plugins
    """
    print "Running connection plugins..."
    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}
    win_xp_2003 = [
        "WinXPSP1x64",
        "WinXPSP2x64",
        "WinXPSP2x86",
        "WinXPSP3x86",
        "Win2003SP0x86",
        "Win2003SP1x64",
        "Win2003SP1x86",
        "Win2003SP2x64",
        "Win2003SP2x86"
        ]
    win_2008_vista_7 = [
        "Win2008R2SP0x64",
        "Win2008R2SP1x64",
        "Win2008SP1x64",
        "Win2008SP1x86",
        "Win2008SP2x64",
        "Win2008SP2x86",
        "Win7SP0x64",
        "Win7SP0x86",
        "Win7SP1x64",
        "Win7SP1x86",
        "VistaSP0x64",
        "VistaSP0x86",
        "VistaSP1x64",
        "VistaSP1x86",
        "VistaSP2x64",
        "VistaSP2x86"]
    plugins = ["connections", "connscan", "sockets", "sockscan"]
    if profile in win_xp_2003:
        output = execute_plugin(params, plugins)

    elif profile in win_2008_vista_7:
        output["netscan"] = execute_plugin(params, ["netscan"])

    return output
