import subprocess
import json
import hashlib
from datetime import datetime

def execute_plugin(params, plugins):
    """ Execute all volatility plugins in plugin list

    Keyword arguments:
    params: parameters for executing the plugin
    :return: json output in a dict
    """

    result = {}
    for plugin in plugins:
        start = datetime.now()
        parameters = ["vol.py", "--output=json", plugin] + params

        print "Running {}...".format(plugin)
        if len(plugins) == 1:
            result = json.loads(subprocess.check_output(parameters, universal_newlines=True))
        else:
            result[plugin] = json.loads(subprocess.check_output(parameters, universal_newlines=True))
        print("{} execution time: {}").format(plugin, datetime.now() - start)
    return result


def execute_dump(params):

    dump = subprocess.Popen(["vol.py"] + params, stdout=subprocess.PIPE)
    dump.wait()


def calculate_hash(path, filename, buffer_size=65536):
    """ Calculate hashes for a given file.

    Keyword arguments:
    path: path to file
    filename: filename of the file
    buffer_size: buffer size for calculating hash (default 64kb)

    :return: dictionary containing array of hashes and filename

    TODO change filename to a hash?
    """

    md5 = hashlib.md5()
    sha1 = hashlib.sha1()
    sha256 = hashlib.sha256()

    with open(path + "/" + filename, 'rb', buffering=0) as infile:
        while True:
            data = infile.read(buffer_size)
            if not data:
                break
            md5.update(data)
            sha1.update(data)
            sha256.update(data)
        hashes = {}
        hashes["md5"] = md5.hexdigest()
        hashes["sha1"] = sha1.hexdigest()
        hashes["sha256"] = sha256.hexdigest()
    hashes["filename"] = filename
    return hashes


def search_array(term, column, array):
    """ Search array for a given term in a given column and return all rows containing that term.

    Keyword arguments:
    term: search term
    column: column to be searched
    array: array to be searched

    :return: dictionary containing names of columns and found rows
    """

    output = {"columns": array["columns"],
              "rows": []}
    try:
        i = array["columns"].index(column)

        for row in array:
            if row[i] == term:
                output["rows"].append(row)
        return output

    except ValueError:
        return None
