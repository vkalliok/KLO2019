# Concourse Volatility Pipeline

Concourse pipeline that finds hidden processes and exports their executables to a git repo with Volatility.

## Quick Deployment

1. Set up [Concourse](https://concourse-ci.org/) ([useful tutorial](https://concoursetutorial.com/))
2. Copy the contents of server folder to your Concourse installation
3. Add the ssh credentials of your output repository to credentials.yml
4. Setup the pipeline with fly:
```
$ fly -t target-name set-pipeline -p pipeline-name -c pipeline.yml -l credentials.yml
```
5. Copy the contents of git folder to repository you'll use for the pipeline and create folders samples/ and dump/ in the same repo
6. Put memory samples to samples/ folder in your repository and trigger the pipeline
