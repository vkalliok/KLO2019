import util


def dumpcerts(sample, profile, output_path, offset=None, pid=None):
    """Dumps RSA private and public SSL keys from memory. Dumps only given process' keys
    if given offset and pid.

    Keyword arguments:
    sample: path to memory sample
    profile: OS memory profile of the sample
    offset: memory offset of the process
    pid: process ID of the process
    output_path: path for output

    :return:
    """
    print "Running certdump..."
    params = ["-f", sample,
              "--profile=" + profile,
              "dumpcerts"
             ]

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    params += ["-D", output_path]
    util.execute_dump(params)

