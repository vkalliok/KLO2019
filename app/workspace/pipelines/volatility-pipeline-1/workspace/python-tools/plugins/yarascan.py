import util
import os

def yarascan(sample, profile, rules):
    # TODO fix everything

    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    for folder in os.listdir(rules):
        for ruleset in os.listdir(rules + "/" + folder):

            if ruleset.endswith(".yar") or ruleset.endswith(".yara"):
                yara_path = "-y " + rules + "/" + folder + "/" + ruleset
                yara_param = params + [yara_path]

                output["yarascan"] = util.execute_plugin(yara_param, ["yarascan"])


    return output