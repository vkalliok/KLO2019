import util

def process_memory(sample, profile, offset=None, pid=None):
    """ Execute memmap, memdump and iehistory (and evt_profile if sample is from correct operating system)
    plugins

    Keyword arguments:
    sample: path to memory sample
    profile: OS memory profile of the sample
    offset: memory offset of the process
    pid: process ID of the process

    :return: output of
    """

    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    plugins = ["memmap", "memdump", "iehistory"]

    evt_profile = ["WinXPSP1x64",
                   "WinXPSP2x64",
                   "WinXPSP2x86",
                   "WinXPSP3x86",
                   "Win2003SP0x86",
                   "Win2003SP1x64",
                   "Win2003SP1x86",
                   "Win2003SP2x64",
                   "Win2003SP2x86"
                   ]

    if profile in evt_profile:
        util.execute_plugin(params, ["evtlogs"])

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["process memory"] = util.execute_plugin(params, plugins)

    return output

def vad(sample, profile, offset=None, pid=None):

    params = ["-f", sample,
              "--profile=" + profile
              ]
    output = {}

    plugins = ["vadinfo", "vadwalk", "vadtree", "vaddump"]

    if pid and offset:
        params += ["-p", pid, "--offset=" + offset]

    output["vad"] = util.execute_plugin(params, plugins)

    return output