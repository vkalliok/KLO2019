import util
def consoles(sample, profile):
    """Execute Volatility's cmdscan and consoles plugins

    Keyword arguments:
    sample: path to memory sample
    profile: OS memory profile of the sample
    :return:
    """

    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}

    output["consoles"] = util.execute_plugin(params, ["consoles"])
    output["cmdscan"] = util.execute_plugin(params, ["cmdscan"])

    return output