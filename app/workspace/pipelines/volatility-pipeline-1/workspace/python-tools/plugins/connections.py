import util

win_xp_2003 = [
    "WinXPSP1x64",
    "WinXPSP2x64",
    "WinXPSP2x86",
    "WinXPSP3x86",
    "Win2003SP0x86",
    "Win2003SP1x64",
    "Win2003SP1x86",
    "Win2003SP2x64",
    "Win2003SP2x86"
]

win_2008_vista_7 = [
    "Win2008R2SP0x64",
    "Win2008R2SP1x64",
    "Win2008SP1x64",
    "Win2008SP1x86",
    "Win2008SP2x64",
    "Win2008SP2x86",
    "Win7SP0x64",
    "Win7SP0x86",
    "Win7SP1x64",
    "Win7SP1x86",
    "VistaSP0x64",
    "VistaSP0x86",
    "VistaSP1x64",
    "VistaSP1x86",
    "VistaSP2x64",
    "VistaSP2x86"]

def connections(sample, profile):
    """ Execute all Volatility plugins pertaining to connections on given memory sample

    connections, connscan, sockets and sockscan work only with OS versions listed in win_xp_2003
    and netscan only on OS versions listed in win_2008_vista_7.

    Keyword arguments:
    sample: path to memory sample
    profile: OS memory profile of the sample

    :return: dictionary containing Volatility JSON output from the executed plugins
    """

    print "Running connection plugins..."
    params = ["-f", sample,
              "--profile=" + profile
             ]
    output = {}


    plugins = ["connections", "connscan", "sockets", "sockscan"]
    if profile in win_xp_2003:
        output = util.execute_plugin(params, plugins)

    elif profile in win_2008_vista_7:
        output["netscan"] = util.execute_plugin(params, ["netscan"])

    return output

def get_profiles():
    return win_xp_2003.append(win_2008_vista_7)
