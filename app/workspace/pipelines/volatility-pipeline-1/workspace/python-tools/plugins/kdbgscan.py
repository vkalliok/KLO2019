import subprocess


def kdbgscan(sample):
    """Execute Volatility's kdbgscan plugin that returns a memory profile suggestion for given
    memory sample

    Keyword arguments:
    sample: path to memory sample

    :return: List of suggested profiles
    """

    print "Running kdbgscan..."
    result = subprocess.check_output(["python", "vol.py", "kdbgscan", "-f", sample], universal_newlines=True)
    result = result.split("\n")
    suggested_profiles = []
    result.pop()

    """
    if len(result) % 16 == 0:
        profiles = [result[x : x + 16] for x in range(0, len(result),16)]
    else:
        return None

    for i in profiles:
        processes = i[9].split("(")[1].split()[0]
        modules = i[10].split("(")[1].split()[0]
        if processes != "0" and modules != "0":
            print i[5].split(":")[1]
            suggested_profiles.append(i[5].split(":")[1].strip())
    """

    processes = None
    modules = None
    found_profile = None

    for line in result:
        print(line)
        if line.startswith("Profile suggestion"):
            found_profile = line.split(":")[1].strip()

        if line.startswith("PsActiveProcessHead"):
            processes = int(line.split("(")[1].split()[0])

        if line.startswith("PsLoadedModuleList"):
            modules = int(line.split("(")[1].split()[0])

        if line.startswith("*"):
            if not (processes == 0 and modules == 0):
                if found_profile != None:
                    suggested_profiles.append(found_profile)

            processes = None
            modules = None
            found_profile = None

    return suggested_profiles
