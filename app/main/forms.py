from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo


class ChangePasswordForm(FlaskForm):
    """ Form for changing user password """
    old_password = PasswordField('Old password', validators=[DataRequired()])
    new_password1 = PasswordField('New password', validators=[DataRequired()])
    new_password2 = PasswordField('Repeat new password', validators=[DataRequired(), EqualTo('new_password1')])

    submit = SubmitField('Change password')
