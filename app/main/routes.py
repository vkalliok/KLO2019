from flask import render_template, send_file, flash
from app.main import bp

from flask_login import login_required, current_user
from app.models import User
from app.main.forms import ChangePasswordForm, ChangeGitLabURIForm
from app.directory_scan import set_metadata, get_sample_by_id, get_artefact_by_id, get_tool_by_name
from flask import Markup
import yaml


@bp.route('/')
@bp.route('/index')
@login_required
def index():
    """ Dashboard """
    metadata = set_metadata()
    return render_template('index.html', title='Dashboard', metadata=metadata)


@bp.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
    """ User page, used for changing user password """
    account = User.query.filter_by(username=username).first_or_404()

    form = ChangePasswordForm()
    form2 = ChangeGitLabURIForm()
    form2.uri_to_gitlab.data = 'http://127.0.0.1:5002'
    if form.validate_on_submit():
        flash('Password changed')

    if form2.validate_on_submit():
        flash('Path to GitLab changed')

    return render_template('user.html', title='User settings', user=account, form=form, form2=form2)


@bp.route('/samples')
@login_required
def samples():
    """ Page listing all samples in environment """
    metadata = set_metadata()
    return render_template('samples.html', metadata=metadata)


@bp.route('/samples/<sample_id>')
@login_required
def sample(sample_id):
    """ Page for single samples """
    metadata = set_metadata()

    sample, data = get_sample_by_id(metadata['samples'], sample_id)

    return render_template('sample.html', sample=sample, data=data)

@bp.route('/samples/<sample_id>/download')
@login_required
def sample_download(sample_id):
    """ Downloading samples to local system """
    metadata = set_metadata()
    try:
        sample, data = get_sample_by_id(metadata['samples'], sample_id)
        sample_path = data['file_path'].split('/', 1)[1:][0]
    except TypeError:
        pass
    try:
        return send_file(sample_path, as_attachment=True)
    except Exception as e:
        print(e)


@bp.route('/results')
@login_required
def results():
    """ Page for all the results (artefacts and reports) found in environment """
    metadata = set_metadata()
    return render_template('results.html', metadata=metadata)


@bp.route('/samples/<sample_id>/results/<tool_name>/<artefact_id>')
@login_required
def result(sample_id, tool_name, artefact_id):
    """ Page for single results (artefacts and reports) """
    metadata = set_metadata()

    sample, data = get_sample_by_id(metadata['samples'], sample_id)
    artefacts = data['tools'][tool_name]['artefacts']

    artefact = get_artefact_by_id(artefacts, artefact_id)

    return render_template('result.html', artefact=artefact, sample=sample, sample_id=sample_id, tool=tool_name)


@bp.route('/samples/<sample_id>/results/<tool_name>/<artefact_id>/download')
@login_required
def result_download(sample_id, tool_name, artefact_id):
    """ Downloading results to local system """
    metadata = set_metadata()
    try:
        sample, data = get_sample_by_id(metadata['samples'], sample_id)
        artefacts = data['tools'][tool_name]['artefacts']

        artefact = get_artefact_by_id(artefacts, artefact_id)
        artefact_path = artefact['path'].split('/', 1)[1:][0]
        print(artefact_path)
    except TypeError:
        pass
    try:
        return send_file(artefact_path, as_attachment=True)
    except Exception as e:
        print(e)


@bp.route('/tools')
@login_required
def tools():
    """ Page for all the tools (tool scripts and pipelines) found in environment """
    metadata = set_metadata()
    return render_template('tools.html', metadata=metadata)


@bp.route('/tools/<tool_name>')
@login_required
def tool(tool_name):
    """ Page for single tools """
    metadata = set_metadata()
    tool = get_tool_by_name(metadata['available_tools'], tool_name)
    artefacts = {}

    for sample, data in metadata['samples'].items():
        try:
            artefacts[sample] = {'sample_id': data['id'], 'artefacts': data['tools'][tool_name]['artefacts']}
        except KeyError:
            pass

    return render_template('tool.html', tool=tool, metadata=metadata, artefacts=artefacts)

@bp.route('/pipelines/<pipeline_name>')
@login_required
def pipeline(pipeline_name):
    """ Page for single pipelines """
    metadata = set_metadata()['pipelines'][pipeline_name]

    pipeline_yaml = yaml.load("".join(metadata['contents']), Loader=yaml.SafeLoader)

    """render = '<p><div class="container"><div class="row">'
    job_count = len(pipeline_yaml['jobs'])
    columns = job_count * 2 + 1
    print("columns: ", 12 / columns)
    for i, job in enumerate(pipeline_yaml['jobs']):
        input = job['plan'][0]['get']
        task = job['plan'][1]['task']
        output = job['plan'][2]['put']
        render += '<div class="panel panel-default panel-warning col-sm-1">{}</div>'.format(input)
        render += '<div class="col-sm-1"><span class="glyphicon glyphicon-arrow-right"></span></div>'
        render += '<div class="panel panel-default panel-info col-sm-1">{}</div>'.format(task)
        render += '<div class="col-sm-1"><span class="glyphicon glyphicon-arrow-right"></span></div>'
        if i == len(pipeline_yaml['jobs'])-1:
            render += '<div class="panel panel-default panel-warning col-sm-1">{}</div>'.format(output)

    render = Markup(render)"""

    return render_template('pipeline.html', metadata=metadata, pipeline_name=pipeline_name) #, render=render)