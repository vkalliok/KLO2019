from app.database import db

from flask_login import UserMixin
from app import login, bcrypt_hash


class User(db.Model, UserMixin):
    """ User database model and password functions """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128), index=True, unique=True)

    def set_password(self, password):
        self.password_hash = bcrypt_hash.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt_hash.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


@login.user_loader
def load_user(id):
    """ Function for getting user by ID """
    return User.query.get(int(id))