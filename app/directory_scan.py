import os
import filetype
import datetime
import hashlib

def date_from_timestamp(timestamp):
    """ Convert epoch timestamp to human readable form """
    return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')

def human_readable_size(size):
    """ Convert file size to human readable form """
    for unit in ['B', 'KB', 'MB', 'GB', 'TB']:
        if abs(size) < 1024.0:
            return '{:.2f}{}'.format(size, unit)
        size /= 1024.0
    return '{:.2f}{}{}'.format(size, 'PB')

def set_tool_artefacts(artefacts_path):
    """ Collect metadata from artefacts produced by a tool in pipeline """
    artefacts = []
    artefact_id = 0
    artefact_count = 0
    log_count = 0

    for artefact in sorted(os.listdir(artefacts_path)):
        contents = None
        artefact_path = os.path.join(artefacts_path, artefact)
        artefact_filesize = os.path.getsize(artefact_path)
        artefact_modified = os.path.getmtime(artefact_path)
        extension = artefact.rsplit(".", 1)[1]
        kind = filetype.guess(artefact_path)

        if extension in ['log', 'json', 'txt']:
            log_count += 1
            with open(artefact_path, 'r') as f:
                contents = f.readlines()
                if len(contents) == 0:
                    contents = None
        elif extension in ['md']:
            log_count += 1
            with open(artefact_path, 'r') as f:
                contents = '\n' + f.read()
                if len(contents) == 0:
                    contents = None
        else:
            artefact_count += 1

        if kind is not None:
            mime = kind.mime
        else:
            mime = "Could not be determined"
        result = {
            "id": artefact_id,
            "path": artefacts_path + artefact,
            "filename": artefact,
            "modified": date_from_timestamp(artefact_modified),
            "hash": artefact.rsplit(".", 1)[0],
            "extension": extension,
            "MIME": mime,
            "contents": contents,
            "filesize": human_readable_size(artefact_filesize),
        }

        artefacts.append(result)
        artefact_id += 1
    print(log_count)
    return artefacts, artefact_count, log_count


def set_metadata(samples_path='app/workspace/samples/',
                 results_path='app/workspace/results/',
                 pipelines_path='app/workspace/pipelines/',
                 tools_path='app/workspace/tools/'):
    """
    Collect metadata of samples, analysis results and artefacts
    from workspace.
    """
    samples = set_sample_metadata(samples_path, results_path)
    pipelines = set_pipeline_metadata(pipelines_path)
    available_tools = set_available_tool_metadata(tools_path)

    metadata = {
        "samples": samples,
        "pipelines": pipelines,
        "available_tools": available_tools
    }
    return metadata


def set_sample_metadata(samples_path, results_path):
    """ Collect metadata for samples """

    samples = {}
    sample_id = 0
    tool_ids = {}
    new_tool_id = 0
    artefact_count = 0
    log_count = 0

    for sample in sorted(os.listdir(samples_path)):
        sample_path = "{}{}".format(samples_path, sample)
        sample_name = sample.split(".")[0]
        file_size = os.path.getsize(sample_path)

        modified = os.path.getmtime(sample_path)
        try:
            extension = sample.rsplit(".", 1)[1]
        except IndexError:
            extension = "undefined"

        kind = filetype.guess(sample_path)
        if kind is not None:
            mime = kind.mime
        else:
            mime = "Could not be determined"

        used_tools = {}
        sample_results_path = "{}{}".format(results_path, sample_name)

        try:
            for tool in sorted(os.listdir(sample_results_path)):
                if tool in tool_ids:
                    tool_id = tool_ids[tool]
                else:
                    tool_id = new_tool_id
                    new_tool_id += 1
                artefacts_path = "{}/{}/".format(sample_results_path, tool)
                artefacts, tool_artefact_count, tool_log_count = set_tool_artefacts(artefacts_path)
                artefact_count += tool_artefact_count
                log_count += tool_log_count
                used_tools[tool] = {
                    "id": tool_id,
                    "name": tool,
                    "artefacts": artefacts
                }
        except FileNotFoundError:
            pass

        samples[sample] = {
            "id": sample_id,
            "modified": date_from_timestamp(modified),
            "tools": used_tools,
            "extension": extension,
            "MIME": mime,
            "artefact_count": artefact_count,
            "log_count": log_count,
            "file_size": human_readable_size(file_size),
            "file_path": sample_path,
        }
        sample_id += 1
    return samples


def set_pipeline_metadata(pipelines_path):
    """ Collect available pipelines in environment """
    pipeline_id = 0
    pipelines = {}

    for pipeline_folder in sorted(os.listdir(pipelines_path)):

        pipeline_path = os.path.join(pipelines_path, pipeline_folder, 'server')

        for pipeline in os.listdir(pipeline_path):
            print(pipeline)
            if pipeline.split('_', 1)[0] == 'pipeline' and pipeline.rsplit('.', 1)[1] == 'yml':
                pipeline_name = pipeline.rsplit(".", 1)[0]
                pipeline_modified = os.path.getmtime(pipeline_path)

                contents = None
                with open(os.path.join(pipeline_path, pipeline), 'r') as f:
                    contents = f.readlines()

                pipeline_data = {
                    "id": pipeline_id,
                    "path": pipeline_path,
                    "modified": date_from_timestamp(pipeline_modified),
                    "contents": contents,
                }
                pipelines[pipeline_name] = pipeline_data
                pipeline_id += 1
    return pipelines


def set_available_tool_metadata(tools_path):
    """ Collect available tools in environment """
    tool_id = 0
    tools = {}
    for tool_name in sorted(os.listdir(tools_path)):

        tool_path = "{}{}".format(tools_path, tool_name)
        task = None
        script = None
        config = None
        for tool in os.listdir(tool_path):
            path = os.path.join(tool_path, tool)
            last_modified = os.path.getmtime(path)
            extension = tool.rsplit(".", 1)[1]
            if extension == "sh":
                task = tool
            elif extension == "py":
                script = tool
            elif extension == "yml":
                config = tool
            with open(path, 'r') as f:
                contents = f.readlines()

        tool = {
            "id": tool_id,
            "name": tool_name,
            "updated": date_from_timestamp(last_modified),
            "tool_path": tool_path,
            "task": task,
            "script": script,
            "config": config,
            "contents": contents,
        }
        tool_id += 1
        tools[tool_name] = tool

    return tools


def print_sample_metadata(samples_metadata):
    """
    List metadata of all samples in workspace.
    Used with CLI.
    """
    print("Available samples:")
    for sample in sorted(samples_metadata.keys()):
        metadata = samples_metadata[sample]

        modified_date = date_from_timestamp(metadata["modified"])

        print("Sample name: {}".format(sample))
        print("\tid: {}".format(metadata["id"]))
        print("\tExtension: {}".format(metadata["extension"]))
        print("\tMime type: {}".format(metadata["MIME"]))
        print("\tModified: {}".format(modified_date))

        if len(metadata["tools"]) > 0:
            print("\n\tTools used\tLatest modification\tNumber of artefacts")
            print("\t" + "-" * 59)

            for tool, values in metadata["tools"].items():
                latest = 0
                for artefact in values["artefacts"]:
                    if artefact["modified"] > latest:
                        latest = artefact["modified"]
                modified_date = datetime.datetime.utcfromtimestamp(latest).strftime('%Y-%m-%dT%H:%M:%SZ')
                print("\t{}\t{}\t{}".format(tool, modified_date, len(values["artefacts"])))

        print("")


def print_available_pipelines(available_pipelines):
    """
    List metadata of all samples in workspace.
    Used with CLI.
    """
    print("Available pipelines:")
    for pipeline in sorted(available_pipelines.keys()):
        metadata = available_pipelines[pipeline]
        modified_date = date_from_timestamp(metadata["modified"])
        print("\t{} {}\t\t{}".format(metadata["id"],
                                     pipeline,
                                     modified_date))
    print("\n")


def print_available_tools(available_tools):
    """
    List available tools in workspace.
    Used with CLI.
    """
    print("Available tools:")
    for tool in sorted(available_tools.keys()):
        print("\t{} {}".format(available_tools[tool]["id"], tool))


def print_tool_artefacts(sample, samples, tool):
    """
    List metadata of artefacts from single tool and single sample.
    Used with CLI.
    """
    tools = samples[sample]["tools"]
    print("Artefacts for {} on {}:".format(tool, sample))
    for artefact in tools[tool]["artefacts"]:
        print_artefact_metadata(artefact)
    print("\n")


def print_artefact_metadata(artefact):
    """
    List metadata of single artefact from single tool and single sample.
    Used with CLI.
    """
    filename = artefact["path"].rsplit("/", 1)[1]
    modified_date = date_from_timestamp(artefact["modified"])

    print("{}".format(filename))
    print("\tid: {}".format(artefact["id"]))
    print("\tExtension: {}".format(artefact["extension"]))
    print("\tMime type: {}".format(artefact["MIME"]))
    print("\tModified: {}".format(modified_date))


def get_sample_by_id(samples, sample_id):
    """ Get all data relating to a single sample by its id """

    for sample, value in samples.items():
        if value["id"] == int(sample_id):
            return sample, value


def get_artefact_by_id(artefacts, artefact_id):
    """ Get all data relating to a single artefact by its id """
    for artefact in artefacts:
        if artefact["id"] == int(artefact_id):
            return artefact


def get_tool_by_id(available_tools, tool_id):
    """ Get single tool by id """
    for tool, values in available_tools.items():
        if values["id"] == tool_id:
            return values


def get_tool_by_name(available_tools, tool_name):
    """ Get single tool by name """
    for tool, values in available_tools.items():
        if values["name"] == tool_name:
            return values
