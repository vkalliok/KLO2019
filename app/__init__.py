from flask import Flask
from config import Config

from flask_bcrypt import Bcrypt
from flask_login import LoginManager

from app.database import db
from flask_migrate import Migrate

from flask_bootstrap import Bootstrap

bcrypt_hash = Bcrypt()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = 'Log in to access this page.'
bootstrap = Bootstrap()


def create_app(config_class=Config):
    """ Create and initialize app """
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)  # initialize database
    login.init_app(app)  # initialize login library
    bcrypt_hash.init_app(app)  # initialize hashing library
    migrate.init_app(app, db)  # initialize db migration library
    bootstrap.init_app(app)  # initialize the main app

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    return app


from app import models
