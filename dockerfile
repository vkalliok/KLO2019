FROM ubuntu:18.04

RUN useradd -d /home/cincan cincan

WORKDIR /home/cincan

COPY requirements.txt requirements.txt

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN buildDeps='build-essential python3-dev' \
    && apt update && apt install -y --no-install-recommends $buildDeps\
    python3 \
    python3-pip \
    python3-setuptools \
    && pip3 install -r requirements.txt \
    && rm -rf /var/lib/apt/lists/* \
    && apt purge -y --auto-remove $buildDeps


COPY app app
COPY migrations migrations
COPY cincan_gui.py config.py boot.sh app.db ./
RUN chmod +x boot.sh

ENV FLASK_APP cincan_gui.py
RUN chown -R cincan:cincan ./
USER cincan

EXPOSE 8000
ENTRYPOINT ["./boot.sh"]
